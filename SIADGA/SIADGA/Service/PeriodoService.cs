﻿using SIADGA.Models;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace SIADGA.Service
{
    public class PeriodoService
    {
        private SIADGAEntities db = new SIADGAEntities();
        public List<Periodo> traePeriodos()
        {
            var periodo = db.Periodo.Include(p => p.Catalogo_Periodo);
            var per = from p in periodo orderby p.IdPeriodo descending select p;
            return per.ToList();
        }

        public Periodo crearPeriodo(Periodo periodo)
        {
            periodo.SaldoAlCorte = 0;
            db.Periodo.Add(periodo);
            db.SaveChanges();
            modificarStatus(periodo.IdPeriodo);
            return periodo;
        }

        private static void modificarStatus(int periodo)
        {
            string cadena = "Initial Catalog=SIADGA; Data Source=DESKTOP-1EF1GSV; Integrated Security = true";
            int parametro = periodo;
            
            using(SqlConnection conn = new SqlConnection(cadena))
            {
                conn.Open();
                string query = "update periodo set status=0 where IdPeriodo<>@idperiodo";
                SqlCommand comando = new SqlCommand(query, conn);
                comando.Parameters.AddWithValue("@idperiodo", parametro);

                comando.ExecuteNonQuery();
            }
        }
        public Periodo getPeriodo(int id)
        {
            return db.Periodo.Find(id);
        }

        public void Actualizar(Periodo periodo)
        {
            db.Entry(periodo).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void ActualizarSaldo(int id)
        {
            ConceptoService conceptoservice = new ConceptoService();
            var conceptos = conceptoservice.traeConceptos(id);
            double? result=0;
            foreach (Concepto concepto in conceptos)
            {
                if(concepto.Pago>=0){
                    result = result + concepto.Pago;
                }
                else
                {
                    result = result - concepto.Abono;
                }
            }

            Periodo periodo = new Periodo();
            periodo = getPeriodo(id);
            periodo.SaldoAlCorte = result;
            Actualizar(periodo);
        }

    }
}