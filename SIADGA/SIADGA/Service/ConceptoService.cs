﻿using SIADGA.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SIADGA.Service
{
    public class ConceptoService
    {
        SIADGAEntities db = new SIADGAEntities();

        public List<Concepto> traeConceptos(int id)
        {
            var conceptos = db.Concepto.ToList();
            var lista = from c in conceptos where c.IdPeriodo == id
                        select new Concepto { IdConcepto = c.IdConcepto, IdPeriodo = c.IdPeriodo, NombreConcepto = c.NombreConcepto, Pago = c.Pago, Abono =  c.Abono };
            
            return (lista.ToList());
        }

        public Concepto crearConcepto(Concepto concepto)
        {
            //double? saldo;
            PeriodoService servicioperiodo = new PeriodoService();
            //Periodo periodo = servicioperiodo.getPeriodo(concepto.IdPeriodo);
            //saldo = periodo.SaldoAlCorte;
            //saldo = saldo + (concepto.Pago + (concepto.Pago - concepto.Abono));
            //periodo.SaldoAlCorte = saldo;
            if (concepto.Abono == null)
            {
                concepto.Abono = 0;
            }
            //servicioperiodo.Actualizar(periodo);
            db.Concepto.Add(concepto);
            db.SaveChanges();
            servicioperiodo.ActualizarSaldo(concepto.IdPeriodo);
            return concepto;
        }

        public Concepto editarConcepto(Concepto concepto)
        {
            //double? saldo;
            PeriodoService servicioperiodo = new PeriodoService();
            //Periodo periodo = servicioperiodo.getPeriodo(concepto.IdPeriodo);
            //saldo = periodo.SaldoAlCorte;
            //saldo = saldo + (concepto.Pago - (concepto.Pago + concepto.Abono));
            //periodo.SaldoAlCorte = saldo;
            if (concepto.Abono == null)
            {
                concepto.Abono = 0;
            }
            //servicioperiodo.Actualizar(periodo);
            db.Entry(concepto).State = EntityState.Modified;
            db.SaveChanges();
            servicioperiodo.ActualizarSaldo(concepto.IdPeriodo);
            return concepto;
        }
    }
}