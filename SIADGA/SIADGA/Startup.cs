﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SIADGA.Startup))]
namespace SIADGA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
