﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SIADGA.Models;
using SIADGA.Service;

namespace SIADGA.Controllers
{
    public class PeriodoesController : Controller
    {
        private SIADGAEntities db = new SIADGAEntities();

        // GET: Periodoes
        public ActionResult Index()
        {
            PeriodoService periodo = new PeriodoService();
            return View(periodo.traePeriodos());
        }

        // GET: Periodoes/Details/5
        //id de periodo
        public ActionResult Details(int id, string nombreperiodo, bool status)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Concepto concepto = db.Concepto.Find(id);
            if (concepto == null)
            {
                return HttpNotFound();
            }*/
            ConceptoService concepto = new ConceptoService();
			ViewBag.nombreperiodo = nombreperiodo;
            ViewBag.IdPer = id;
            ViewBag.status = status;
            return View(concepto.traeConceptos(id));
        }

        // GET: Periodoes/Create
        public ActionResult Create()
        {
            ViewBag.IdCatalogoPeriodo = new SelectList(db.Catalogo_Periodo, "ID_CatPeriodo", "Descripcion");
            return View();
        }

        // POST: Periodoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Periodo periodo)
        {
            /*if (ModelState.IsValid)
            {*/
                PeriodoService servicio = new PeriodoService();
                servicio.crearPeriodo(periodo);
                return RedirectToAction("Index");
            //}

            ViewBag.IdCatalogoPeriodo = new SelectList(db.Catalogo_Periodo, "ID_CatPeriodo", "Descripcion", periodo.IdCatalogoPeriodo);
            return View(periodo);
        }

        // GET: Periodoes/Edit/5
        //id de concepto
        public ActionResult Edit(int id, string nombreperiodo, bool status)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Concepto concepto = db.Concepto.Find(id);            
            if (concepto == null)
            {
                return HttpNotFound();
            }
            ViewBag.nombreperiodo = nombreperiodo;
            ViewBag.status = status;
            //ViewBag.IdCatalogoPeriodo = new SelectList(db.Catalogo_Periodo, "ID_CatPeriodo", "Descripcion", periodo.IdCatalogoPeriodo);
            return View(concepto);
        }

        // POST: Periodoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Concepto concepto)
        {
            if (ModelState.IsValid)
            {
                /*db.Entry(concepto).State = EntityState.Modified;
                db.SaveChanges();*/
                ConceptoService servicio = new ConceptoService();
                servicio.editarConcepto(concepto);
                return RedirectToAction("Index");
            }
            //ViewBag.IdCatalogoPeriodo = new SelectList(db.Catalogo_Periodo, "ID_CatPeriodo", "Descripcion", periodo.IdCatalogoPeriodo);
            return View(concepto);
        }

        // GET: Periodoes/Create
        public ActionResult CreateConcepto(int idPeriodo)
        {
            ViewBag.IdPeriodo = idPeriodo;
            return View();
        }

        // POST: Periodoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateConcepto(Concepto concepto)
        {
            /*if (ModelState.IsValid)
            {*/
                ConceptoService servicio = new ConceptoService();
                servicio.crearConcepto(concepto);
                //servicio.crearPeriodo(concepto);
                return RedirectToAction("Index");
            //}

            //ViewBag.IdCatalogoPeriodo = new SelectList(db.Catalogo_Periodo, "ID_CatPeriodo", "Descripcion", periodo.IdCatalogoPeriodo);
            return View(concepto);
        }

        // GET: Periodoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Periodo periodo = db.Periodo.Find(id);
            if (periodo == null)
            {
                return HttpNotFound();
            }
            return View(periodo);
        }

        // POST: Periodoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Periodo periodo = db.Periodo.Find(id);
            db.Periodo.Remove(periodo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
